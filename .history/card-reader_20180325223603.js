'use strict';

// LIB
var pcsc = require('pcsclite');
const legacy = require('legacy-encoding');
const fs = require('fs');

// CONST
const SELECT = [0x00, 0xA4, 0x04, 0x00, 0x08]
const THAI_CARD = [0xA0, 0x00, 0x00, 0x00, 0x54, 0x48, 0x00, 0x01]
const CMD_SELECT = new Buffer( SELECT.concat(THAI_CARD) );

const CMD_CID = new Buffer([0x80, 0xb0, 0x00, 0x04, 0x02, 0x00, 0x0d]);
const CMD_FULLNAME = new Buffer([0x80, 0xb0, 0x00, 0x11, 0x02, 0x00, 0xd1]);
const CMD_FULLNAME_TH = new Buffer([0x80, 0xb0, 0x00, 0x11, 0x02, 0x00, 0x64]);
const CMD_FULLNAME_EN = new Buffer([0x80, 0xb0, 0x00, 0x75, 0x02, 0x00, 0x64]);
const CMD_DATE_OF_BIRTH = new Buffer([0x80, 0xb0, 0x00, 0xD9, 0x02, 0x00, 0x08]);
const CMD_GENDER = new Buffer([0x80, 0xb0, 0x00, 0xE1, 0x02, 0x00, 0x01]);
const CMD_CARD_ISSUER = new Buffer([0x80, 0xb0, 0x00, 0xF6, 0x02, 0x00, 0x64]);
const CMD_ISSUE_DATE = new Buffer([0x80, 0xb0, 0x01, 0x67, 0x02, 0x00, 0x08]);
const CMD_EXPIRY_DATE = new Buffer([0x80, 0xb0, 0x01, 0x6F, 0x02, 0x00, 0x08]);
const CMD_ADDRESS = new Buffer([0x80, 0xb0, 0x15, 0x79, 0x02, 0x00, 0x64]);

var pcsc = pcsc();

pcsc.on('reader', function(reader){

	let cmdReq = [];

	console.log('Reader detected [', reader.name, ']');
    reader.on('error', function(err) {
        console.log('Error(', this.name, '):', err.message);
    });

    reader.on('status', function(status) {
        if(status.atr[0] == 59 && status.atr[0] == 103){
        	cmdReq = [0x00, 0xc0, 0x00, 0x01];
        }else{
        	cmdReq = [0x00, 0xc0, 0x00, 0x00];
        }
        console.log('status',cmdReq);

        var changes = this.state ^ status.state;
    	if (changes) {
	    	/* card removed */
	    	if ((changes & this.SCARD_STATE_EMPTY) && (status.state & this.SCARD_STATE_EMPTY)) {
 				console.log("card removed");
                reader.disconnect(reader.SCARD_LEAVE_CARD, function(err) {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log('Disconnected');
                    }
                });
	    	}
			/* card inserted */
	    	else if ((changes & this.SCARD_STATE_PRESENT) && (status.state & this.SCARD_STATE_PRESENT)) {
                console.log("card inserted");

                reader.connect({ share_mode : this.SCARD_SHARE_SHARED }, function(err, protocol) {
                    if (err) {
                        console.log(err);
                    } else {
                        readInfo(protocol).then((result) => {
						  console.log(':::: DONE ::::');
						});
                    }
                });
            }
	    }

    });

    // Async function 
    async function readInfo(protocol){
		let info = {};
		selectCard(protocol);
		info['id'] = await readData(CMD_CID, protocol);
		info['fullname'] = await readData(CMD_FULLNAME, protocol);
		info['thfullname'] = await readData(CMD_FULLNAME_TH, protocol);
		info['enfullname'] = await readData(CMD_FULLNAME_EN, protocol);
		info['address'] = await readData(CMD_ADDRESS, protocol);
		info['gender'] = await readData(CMD_GENDER, protocol);
		info['date_of_birth'] = await readData(CMD_DATE_OF_BIRTH, protocol);
		info['card_issuer'] = await readData(CMD_CARD_ISSUER, protocol);
		info['issuer_date'] = await readData(CMD_ISSUE_DATE, protocol);
		info['card_expiry'] = await readData(CMD_EXPIRY_DATE, protocol);

		info['image'] = await readImage(protocol);
		console.log(':::: INFO ::::',info);
	}

	function selectCard(protocol){
		return new Promise((resolve, reject) => {
			reader.transmit(CMD_SELECT, 255, protocol, function(err, data) {
				if (err) {
					reject(err);
				} else {
					resolve(String(data));
				}
			});
		});
	}


	function readData(cmd, protocol){
		return new Promise((resolve, reject) => {
			reader.transmit(cmd, 255, protocol, function(err, data) {
				if (err) {
					reject(err);
				} else {
					console.log(cmd,data);
					var cmd2 = cmdReq.concat([data[1]]);
					reader.transmit(new Buffer(cmd2), 255, protocol, function(err, data) {
						if (err) {
							reject(err);
						} else {
							cleanFormat(data).then((res)=>{
								resolve(res);
							});
						}
					});
				}
			});
		});
	}

	function readImage(protocol){
		console.log('readImage');
		return new Promise((resolve, reject) => {
			let checkMod=0;
			let imgTemp = '';
			let cmdIndex = 0;
			let inGetImage = false;

			readImageData(protocol);

			function readImageData(protocol){
				let ccc = 255;
				let xwd;
			    let xof = (cmdIndex) * ccc + 379;
			      if (cmdIndex == 20)
			         xwd = 38;
			     else 
			     	xwd = ccc;

			    let sp2 = (xof >> 8) & 0xff;
			    let sp3 = xof & 0xff;
			    let sp6 = xwd & 0xff;
			    let spx = xwd & 0xff;
			    var CMD1 = new Buffer([0x80, 0xb0, sp2, sp3, 0x02, 0x00, sp6]);
			    var CMD2 = new Buffer([0x00, 0xc0, 0x00, 0x00, sp6]);

				reader.transmit(CMD1, 257, protocol, function(err, data) {
					if (err) {
						reject(err);
					} else {
						reader.transmit(CMD2, 257, protocol, function(err, data) {
							if (err) {
								console.error('Error!!!',err);
							} else {
								// console.log(data.toString('base64'),'+++++++');
								// console.log(data.slice(0,-1).toString('base64'),'+++++++');
								// imgTemp = imgTemp + data;
								// imgTemp = imgTemp + data.slice(0,-2).toString('base64');
			            		checkMod++;
								if (cmdIndex < 20) {
			                        ++cmdIndex;
			                        inGetImage = true;
			                        readImageData(protocol);
			                    } else {
			                    	resolve(imgTemp);
			                    }
							}
						});
					}
				});
			}
		});

		
	}

    reader.on('end', function() {
        console.error('Reader',  this.name, 'removed');
    });

    function exit() {
		reader.close();
		pcsc.close();
	}
});

function cleanFormat(str){
	return new Promise((resolve) => {
		str = legacy.decode(str, "tis620");
		str = str.substring(0, str.length-2);
		str = str.replace(/\0/gi, ''); // ลบ​ \u0000
		resolve(str);
	});
}




